const express = require('express');
const logger = require('../utils/logger');
const db = require('../database/load-database')

const router = express.Router();

router.get('/' , (req, res, next) => {
    db.query('Select * FROM directors' , (err , data) => {
        if (err) {
            logger.error(err);
            res.status(500).json({msg: 'Some Internal error'});
        }else {
            res.json(data);
        }
    })
})
router.post('/' , (req, res, next) => {
    name = req.body.name;
    if (name == undefined) {
        return res.status(400).json({msg: 'Bad request'});
    }
    db.query('INSERT INTO directors (director_name) VALUE (\"' + name + '\")' , (err, data) => {
        if (err) {
            console.log(err);
            logger.error(err);
            res.status(500).json({msg: 'some internal error'});
        }else {
            res.json({msg: 'Director Added'})
        }
    })
})

router.get('/:id' , (req, res, next) => {
    const id = req.params.id;
    if (isNaN(id)) {
        res.status(400).json({msg: 'bad request'});
    }else {
        db.query('SELECT * FROM directors WHERE id =' + id , (err, data) => {
            if (err) {
                logger.error(err);
            }else {
                if (data.length == 0) {
                    return res.json({msg: 'Director do not not exist'});
                }
                return res.json(data);
            }
        })
    }
})
router.post('/:id' , (req, res, next) => {
    const name = req.body.name;
    const id = req.params.id;
    if (/\d/.test(name) || name == undefined || name.length<1 || isNaN(id)) {
        return res.status(400).json({msg: 'Bad request'});
    }else {
        db.query('UPDATE directors SET director_name =\"'+ name+'\" WHERE id = \"'+ id+'\"' , (err, data)=> {
            if (err) {
                logger.error(err);
                res.status(500).json({msg: 'Some Internal Error'});
            }else {
                res.json({msg: 'Director Updated'});
            }
        });
    }
})
router.delete('/:id' , (req, res, next) => {
    const id = req.params.id;
    if (isNaN(id)) {
        res.status(400).json({msg: 'Bad Request'});
    }else {
        db.query('DELETE FROM directors WHERE id =' + id , (err , data) => {
            if (err) {
                logger.error(err);
                res.status(500).json({msg: 'Some internal Error'});
            }else {
                if (data.length == 0) {
                    return res.json({msg: 'director do not not exist'});
                }
                res.json({msg: "Director Deleted"});
            }
        })
    }
})

module.exports = router;
