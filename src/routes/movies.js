const express = require('express');
const logger = require('../utils/logger');
const db = require('../database/load-database');

const router = express.Router();

router.get('/' , (req, res, next) => {
    db.query('Select * FROM movies' , (err , data) => {
        if (err) {
            logger.error(err);
            res.status(500).json({msg: 'Some Internal error'});
        }else {
            res.json(data);
        }
    })
})
router.post('/' , (req, res, next) => {
    const keyAndValues = Object.entries(req.body);
    let query = "INSERT INTO movies ("
    keyAndValues.forEach((arr, index) => {
        if (index+1 !== keyAndValues.length) {
            query = query  +arr[0]+",";
        }else {
            query = query +arr[0];
        }
    })
    query = query + ") VALUES (";
    keyAndValues.forEach((arr, index) => {
        if (index+1 !== keyAndValues.length) {
            if (arr[0] == 'runtime' ||arr[0] == 'rating' || arr[0] == 'metascore' || arr[0] == 'votes' || arr[0] == 'year'|| arr[0] == 'director') {
                query = query +parseInt(arr[1])+",";
            }else {
                query = query + "\""+arr[1]+"\",";
            }
        }else {
            if (arr[0] == 'runtime' ||arr[0] == 'rating' || arr[0] == 'metascore' || arr[0] == 'votes' || arr[0] == 'release_year' || arr[0] == 'director') {
                query = query +parseInt(arr[1])+"";
            }else {
                query = query + "\""+arr[1]+"\"";
            }
        }
    })
    query = query + ");";
    console.log(query);
    db.query(query , (err, data) => {
        if (err) {
            logger.error(err);
            res.status(500).json({msg: 'Some internal Error'});
        }else {
            res.json({msg: 'Movie Added'});
        }
    })
})

router.get('/:id' , (req, res, next) => {
    const id = req.params.id;
    if (isNaN(id)) {
        res.status(400).json({msg: 'bad request'});
    }else {
        db.query('SELECT * FROM movies WHERE id =' + id , (err, data) => {
            if (err) {
                logger.error(err);
            }else {
                if (data.length == 0) {
                    return res.json({msg: 'Movie id do not not exist'});
                }
                return res.json(data);
            }
        })
    }
})
router.post('/:id' , (req, res, next) => {
    const keyAndValues = Object.entries(req.body);
    const id = req.params.id;
    let query = "UPDATE movies SET "
    keyAndValues.forEach((arr, index) => {
        if (index+1 !== keyAndValues.length) {
            if (arr[0] == 'runtime' ||arr[0] == 'rating' || arr[0] == 'metascore' || arr[0] == 'votes' || arr[0] == 'year'|| arr[0] == 'director') {
                query = query  +arr[0]+" = " + parseInt(arr[1]) + ",";
            }else {
                query = query  +arr[0]+" = " +"\""+ arr[1]+"\"" + ",";
            }
        }else {
            if (arr[0] == 'runtime' ||arr[0] == 'rating' || arr[0] == 'metascore' || arr[0] == 'votes' || arr[0] == 'year'|| arr[0] == 'director') {
                query = query  +arr[0]+" = " + parseInt(arr[1]) ;
            }else {
                query = query  +arr[0]+" = " +"\""+ arr[1]+"\"" ;
            }
        }
    })
    query = query + " WHERE id = "+id;
    console.log(query);
    db.query(query , (err, data) => {
        if (err) {
            logger.error(err);
            res.status(500).json({msg: 'Some internal Error'});
        }else {
            res.json({msg: 'Movie Updated'});
        }
    })
})
router.delete('/:id' , (req, res, next) => {
    const id = req.params.id;
    if (isNaN(id)) {
        res.status(400).json({msg: 'Bad Request'});
    }else {
        db.query('DELETE FROM movies WHERE id =' + id , (err , data) => {
            if (err) {
                logger.error(err);
                res.status(500).json({msg: 'Some internal Error'});
            }else {
                if (data.affectedRows == 0) {
                    return res.json({msg: 'Movie do not not exist'});
                }
                res.json({msg: "Movie Deleted"});
            }
        })
    }
})

module.exports = router;
