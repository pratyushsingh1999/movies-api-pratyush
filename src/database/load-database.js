const mysql = require('mysql2');
require('dotenv').config();
const logger = require('../utils/logger');
const path = require('path');
const fileReader = require('../utils/readFromFile');
const dataFilePath = path.join(__dirname, '..', 'data', 'movies.json');

const connection = mysql.createPool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME
})
connection.query('SELECT * FROM movies', (err, data) => {
    if (err) {
        connection.query('CREATE TABLE movies (\n' +
            'id INT PRIMARY KEY AUTO_INCREMENT,\n' +
            'title Text,\n' +
            'description_of_movie TEXT,\n' +
            'runtime INT,\n' +
            'genre TEXT,\n' +
            'rating FLOAT,\n' +
            'metascore TEXT,\n' +
            'votes BIGINT,\n' +
            'gross_earning_in_mil TEXT,\n' +
            'director INT REFERENCES directors(id),\n' +
            'actor TEXT,\n' +
            'release_year INT\n' +
            ');', (err, data) => {
                if (err) {
                    logger.error(err);
                }else {
                    connection.query('SELECT * FROM directors' , (err , data) => {
                        if (err) {
                            connection.query(`CREATE TABLE directors (
                                             id INT PRIMARY KEY AUTO_INCREMENT,
                                             director_name TEXT
                                             )`, (err , data) => {
                                if (err) {
                                    logger.error(err);
                                }else {
                                    loadData();
                                }
                            })
                        }
                    })
                }
            }
        )
    }
})
const loadData = () => {
    fileReader(dataFilePath)
        .then((data) => {
            data = JSON.parse(data);
            let value = [];
            let directors = [];
            let id;
            data.forEach((movies) => {
                if (!directors.includes(movies.Director)) {
                    directors.push(movies.Director);
                }else {
                }
            })
            directors = directors.map((director , index) => {
                return [index+1 , director];
            })
            data.forEach((movies) => {
                directors.forEach((director) => {
                    if (director[1] === movies.Director) {
                        id = director[0];
                    }
                })
                value.push([movies.Title, movies.Description , movies.Runtime , movies.Genre , movies.Rating , movies.Metascore , movies.Votes , movies.Gross_Earning_in_Mil , id , movies.Actor , movies.Year]);
            })
            const queryForMovies = `INSERT INTO movies (title ,
                description_of_movie ,
                runtime ,
                genre ,
                rating ,
                metascore ,
                votes ,
                gross_earning_in_mil ,
                director ,
                actor , 
                release_year) VALUES ?`
            connection.query(queryForMovies , [value] , (err , data) => {
                if (err) {
                    logger.error(err);
                }else {
                    logger.info("Movies loaded successfully in database");
                }
            })
            const queryForDirectors = `INSERT INTO directors (id , director_name) VALUES ?`
            connection.query(queryForDirectors, [directors] , (err, data) => {
                if (err) {
                    logger.error(err);
                }
                else {
                    logger.info("Successfully Loaded directors table");
                }
            })
        })
        .catch((err) => {
            console.log('error', err);
        })
}

module.exports = connection;
