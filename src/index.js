const express = require('express');
const app = express();
require("dotenv").config();
const PORT = process.env.PORT || 8000;
const logger = require('./utils/logger');
const bodyParser = require('body-parser');

const moviesRoute = require('./routes/movies');
const directorsRoute = require('./routes/directors');

app.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));
app.use(bodyParser.json({ limit: "50mb" }));

app.use('/api/movies' , moviesRoute);
app.use('/api/directors' , directorsRoute);

app.use((req, res, next) => {
    res.status(404).send('Error page not found');
})
app.listen(PORT ,() => {
    if (process.env.ENV == 'development') {
        logger.info(`Server live at http://localhost:${PORT}`);
    }
})
